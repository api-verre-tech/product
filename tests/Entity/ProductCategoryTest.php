<?php

namespace tests\Entity;

use App\Entity\ProductCategory;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductCategoryTest extends KernelTestCase
{
    public function testProductCategoryCreate()
    {
        $product_category = new ProductCategory();
        $product_category->setName("TEST");
        $product_category->setOffersProduct(true);

        $this->assertEquals("TEST", $product_category->getName());
        $this->assertEquals(true, $product_category->getOffersProduct());
        $this->assertEquals(new ArrayCollection(), $product_category->getCategories());
    }
}
