<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OpinionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateTime;

/**
 * @ApiResource()
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "fkid_u": "exact"})
 * @ORM\Entity(repositoryClass=OpinionRepository::class)
 */
class Opinion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"product:read", "product:write","product_categ:read"})
     */
    private $fkid_u;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="opinions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fkid_p;

    /**
     * @ORM\Column(type="string", length=512)
     * @Groups({"product:read", "product:write","product_categ:read"})
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"product:read", "product:write","product_categ:read"})
     */
    private $add_time;

    /**
     * @ORM\Column(type="float")
     * @Groups({"product:read", "product:write","product_categ:read"})
     */
    private $score;

    public function __construct()
    {
        $this->add_time = new DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkidU(): ?int
    {
        return $this->fkid_u;
    }

    public function setFkidU(int $fkid_u): self
    {
        $this->fkid_u = $fkid_u;

        return $this;
    }

    public function getFkidP(): ?Product
    {
        return $this->fkid_p;
    }

    public function setFkidP(?Product $fkid_p): self
    {
        $this->fkid_p = $fkid_p;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAddTime(): ?\DateTimeInterface
    {
        return $this->add_time;
    }

    public function setAddTime(\DateTimeInterface $add_time): self
    {
        $this->add_time = $add_time;

        return $this;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function setScore(float $score): self
    {
        $this->score = $score;

        return $this;
    }
}
