<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    public function testApiPlateform()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        
        // This link HAS TO BE protected in production
        // Every MS that call product has to be authenticated
        // real URI = /api/docs
        $client->request('GET', '/api');
        
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        //$this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', '/api/docs');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString(
            'Product',
            $client->getResponse()->getContent(),
            "La documentation API Platform ne liste pas l'entité User"
        );

        $this->assertStringContainsString(
            'ProductCategory',
            $client->getResponse()->getContent(),
            "La documentation API Platform ne liste pas l'entité User"
        );
    }
}
