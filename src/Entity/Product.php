<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     attributes={"pagination_client_items_per_page"=true},
 *     normalizationContext={"groups"={"product:read"}},
 *     denormalizationContext={"groups"={"product:write"}},
 *     collectionOperations={
 *        "get",
 *        "post"
 *     },
 *     itemOperations={
 *        "get",
 *        "put",
 *        "delete",
 *        "patch",
 *        "put_image"={
 *             "method"="put",
 *             "path"="/products/{id}/uploadImage",
 *             "openapi_context"={
 *                 "requestBody"={
 *                      "content"={
 *                          "application/json"={
 *                              "required"=true,
 *                              "schema"={
 *                                  "type"="object",
 *                                  "required"={"id"},
 *                                  "properties"={
 *                                      "id"={"type"="integer","example"="1"}
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  },
 *                 "responses"={
 *                     "200"={
 *                         "description"="Image ressource updated",
 *                         "content"={
 *                             "application/ld+json"={
 *                                 "schema"={
 *                                     "type"="object",
 *                                     "required"={"id","pictures"},
 *                                     "properties"={
 *                                         "id"={"type"="integer","example"="1"},
 *                                         "pictures"={"type"="integer","example"="img"}
 *                                     }
 *                                 },
 *                             },
 *                         },
 *                     }
 *                 }
 *             }
 *         }
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "name": "partial", "description": "partial", "fkIdProdCat": "end", "price": "start"})
 * @ApiFilter(PropertyFilter::class)
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"product:read","product_categ:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Type("string")
     * @Groups({"product:read", "product:write","product_categ:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="array")
     * @Assert\Type("array")
     * @Groups({"product:read", "product:write","product_categ:read"})
     */
    private $pictures = [];

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Type("string")
     * @Groups({"product:read", "product:write","product_categ:read"})
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     * @Assert\Type("float")
     * @Groups({"product:read", "product:write","product_categ:read"})
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=ProductCategory::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"product:read", "product:write"})
     */
    private $fkIdProdCat;

    /**
     * @ORM\OneToMany(targetEntity=Opinion::class, mappedBy="fkid_p", orphanRemoval=true)
     * @Groups({"product:read", "product:write","product_categ:read"})
     */
    private $opinions;

    public function __construct()
    {
        $this->opinions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPictures(): ?array
    {
        return $this->pictures;
    }

    public function setPictures(array $pictures): self
    {
        $this->pictures = $pictures;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getFkidProdcat(): ?ProductCategory
    {
        return $this->fkIdProdCat;
    }

    public function setFkidProdcat(?ProductCategory $fkIdProdCat): self
    {
        $this->fkIdProdCat = $fkIdProdCat;

        return $this;
    }

    /**
     * @return Collection|Opinion[]
     */
    public function getOpinions(): Collection
    {
        return $this->opinions;
    }

    public function addOpinion(Opinion $opinion): self
    {
        if (!$this->opinions->contains($opinion)) {
            $this->opinions[] = $opinion;
            $opinion->setFkidP($this);
        }

        return $this;
    }

    public function removeOpinion(Opinion $opinion): self
    {
        if ($this->opinions->removeElement($opinion)) {
            // set the owning side to null (unless already changed)
            if ($opinion->getFkidP() === $this) {
                $opinion->setFkidP(null);
            }
        }

        return $this;
    }
}
