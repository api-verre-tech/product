<?php

namespace tests\Entity;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductTest extends KernelTestCase
{
    public function testProductCreate()
    {
        $product = new Product();
        $product->setDescription("TEST");
        $product->setName("TEST");
        $product->setPictures([]);
        $product->setPrice(89.9);

        $this->assertEquals("TEST", $product->getDescription());
        $this->assertEquals("TEST", $product->getName());
        $this->assertEquals(null, $product->getFkidProdcat());
        $this->assertEquals([], $product->getPictures());
        $this->assertEquals(89.9, $product->getPrice());
    }
}
