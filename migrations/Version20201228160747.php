<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201228160747 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, fkIdProdCat_id INT NOT NULL, name VARCHAR(255) NOT NULL, pictures LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', description VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_D34A04AD34C734DB (fkIdProdCat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, offers_product TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE LinkCategory (parent INT NOT NULL, child INT NOT NULL, INDEX IDX_EE251BE73D8E604F (parent), INDEX IDX_EE251BE722B35429 (child), PRIMARY KEY(parent, child)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD34C734DB FOREIGN KEY (fkIdProdCat_id) REFERENCES product_category (id)');
        $this->addSql('ALTER TABLE LinkCategory ADD CONSTRAINT FK_EE251BE73D8E604F FOREIGN KEY (parent) REFERENCES product_category (id)');
        $this->addSql('ALTER TABLE LinkCategory ADD CONSTRAINT FK_EE251BE722B35429 FOREIGN KEY (child) REFERENCES product_category (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD34C734DB');
        $this->addSql('ALTER TABLE LinkCategory DROP FOREIGN KEY FK_EE251BE73D8E604F');
        $this->addSql('ALTER TABLE LinkCategory DROP FOREIGN KEY FK_EE251BE722B35429');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_category');
        $this->addSql('DROP TABLE LinkCategory');
    }
}
