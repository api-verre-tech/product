#!/bin/bash
DOCKER=docker
if [ $# -eq 0 ]; then
    echo 'Il manque le nom du microservice!'
else
    MYSQL_RUNNING=`$DOCKER service ps $1_mysql | awk '{print $6}' | grep Running`
    while [ ${#MYSQL_RUNNING} = 0 ]; do
        MYSQL_RUNNING=`$DOCKER service ps $1_mysql | awk '{print $6}' | grep Running`
    done
fi
