<?php

namespace App\DataFixtures;

use App\Entity\Opinion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Product;
use App\Entity\ProductCategory;
use DateTime;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Création de toutes les catégories de base
        $categ_porteFenetre = new ProductCategory();
        $categ_porteFenetre->setName("Porte fenêtre");
        $categ_porteFenetre->setOffersProduct(false);
        $manager->persist($categ_porteFenetre);
        
        $categ_fenetre = new ProductCategory();
        $categ_fenetre->setName("Fenêtre");
        $categ_fenetre->setOffersProduct(false);
        $manager->persist($categ_fenetre);
        
        $categ_baieVitree = new ProductCategory();
        $categ_baieVitree->setName("Baie vitrée");
        $categ_baieVitree->setOffersProduct(false);
        $manager->persist($categ_baieVitree);
        
        $categ_porteEntree = new ProductCategory();
        $categ_porteEntree->setName("Porte d'entrée");
        $categ_porteEntree->setOffersProduct(false);
        $manager->persist($categ_porteEntree);

        $img = base64_encode(file_get_contents("https://i.pinimg.com/originals/3b/2a/e6/3b2ae6505d498bfc2dc987774c3e3c5c.jpg"));
        
        /**************** PORTE FENETRE ****************/
        if (true) {
            /****** PVC *****/
            $subcateg_PVC = new ProductCategory();
            $subcateg_PVC->setName("PVC");
            
            $product = new Product();
            $product->setDescription("220x200");
            $product->setName("Porte fenêtre PVC");
            $product->setPrice(590.99);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);
            $manager->persist($product);

            $opi = new Opinion();
            $opi->setFkidU(1);
            $opi->setFkidP($product);
            $opi->setComment("Produit conforme aux attentes.");
            $opi->setAddTime(new DateTime('now'));
            $opi->setScore(4);
            $manager->persist($opi);

            $opi = new Opinion();
            $opi->setFkidU(2);
            $opi->setFkidP($product);
            $opi->setComment("Les gonds n'ont pas tenu.");
            $opi->setAddTime(new DateTime('now'));
            $opi->setScore(1);
            $manager->persist($opi);

            $product = new Product();
            $product->setDescription("100x150");
            $product->setName("Porte fenêtre PVC");
            $product->setPrice(380.59);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);

            $opi = new Opinion();
            $opi->setFkidU(2);
            $opi->setFkidP($product);
            $opi->setComment("Produit conforme aux attentes.");
            $opi->setAddTime(new DateTime('now'));
            $opi->setScore(4);

            $manager->persist($product);
            $manager->persist($opi);

            $product = new Product();
            $product->setDescription("300x200");
            $product->setName("Porte fenêtre PVC");
            $product->setPrice(680.99);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);

            $opi = new Opinion();
            $opi->setFkidU(3);
            $opi->setFkidP($product);
            $opi->setComment("Produit conforme aux attentes.");
            $opi->setAddTime(new DateTime('now'));
            $opi->setScore(4);
            
            $manager->persist($product);
            $manager->persist($opi);

            $subcateg_PVC->setOffersProduct(true);
            $categ_porteFenetre->addCategory($subcateg_PVC);
            $manager->persist($subcateg_PVC);
            /******************/


            /****** PVC-Alu *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("PVC-Alu");
            
            $product = new Product();
            $product->setDescription("110x150");
            $product->setName("Porte fenêtre PVC-Alu");
            $product->setPrice(690.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x150");
            $product->setName("Porte fenêtre PVC-Alu");
            $product->setPrice(470.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x250");
            $product->setName("Porte fenêtre PVC-Alu");
            $product->setPrice(580.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_porteFenetre->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            /******************/
            
            /****** Bois *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("Bois");
            
            $product = new Product();
            $product->setDescription("300x100");
            $product->setName("Porte fenêtre Bois");
            $product->setPrice(850.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x100");
            $product->setName("Porte fenêtre Bois");
            $product->setPrice(720.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("400x250");
            $product->setName("Porte fenêtre Bois");
            $product->setPrice(1220.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_porteFenetre->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            /******************/
            
            /****** Bois-Alu *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("Bois-Alu");
            
            $product = new Product();
            $product->setDescription("90x100");
            $product->setName("Porte fenêtre Bois-Alu");
            $product->setPrice(220.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x100");
            $product->setName("Porte fenêtre Bois-Alu");
            $product->setPrice(390.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("300x300");
            $product->setName("Porte fenêtre Bois-Alu");
            $product->setPrice(450.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_porteFenetre->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            $manager->persist($categ_porteFenetre);
            /******************/
        }
        /********************************************************/

        

        /**************** FENETRE ****************/
        if (true) {
            /****** PVC *****/
            $subcateg_PVC = new ProductCategory();
            $subcateg_PVC->setName("PVC");
            
            $product = new Product();
            $product->setDescription("220x200");
            $product->setName("Fenêtre PVC");
            $product->setPrice(590.99);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);
            $manager->persist($product);

            $product = new Product();
            $product->setDescription("100x150");
            $product->setName("Fenêtre PVC");
            $product->setPrice(380.59);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);
            $manager->persist($product);

            $product = new Product();
            $product->setDescription("300x200");
            $product->setName("Fenêtre PVC");
            $product->setPrice(680.99);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);
            $manager->persist($product);

            $subcateg_PVC->setOffersProduct(true);
            $categ_fenetre->addCategory($subcateg_PVC);
            $manager->persist($subcateg_PVC);
            /******************/


            /****** PVC-Alu *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("PVC-Alu");
            
            $product = new Product();
            $product->setDescription("110x150");
            $product->setName("Fenêtre PVC-Alu");
            $product->setPrice(690.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x150");
            $product->setName("Fenêtre PVC-Alu");
            $product->setPrice(470.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x250");
            $product->setName("Fenêtre PVC-Alu");
            $product->setPrice(580.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_fenetre->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            /******************/
            
            /****** Bois *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("Bois");
            
            $product = new Product();
            $product->setDescription("300x100");
            $product->setName("Fenêtre Bois");
            $product->setPrice(850.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x100");
            $product->setName("Fenêtre Bois");
            $product->setPrice(720.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("400x250");
            $product->setName("Fenêtre Bois");
            $product->setPrice(1220.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_fenetre->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            /******************/
            
            /****** Bois-Alu *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("Bois-Alu");
            
            $product = new Product();
            $product->setDescription("90x100");
            $product->setName("Fenêtre Bois-Alu");
            $product->setPrice(220.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x100");
            $product->setName("Fenêtre Bois-Alu");
            $product->setPrice(390.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("300x300");
            $product->setName("Fenêtre Bois-Alu");
            $product->setPrice(450.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_fenetre->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            $manager->persist($categ_fenetre);
            /******************/
        }
        /********************************************************/


        /**************** BAIE VITREE ****************/
        if (true) {
            /****** PVC *****/
            $subcateg_PVC = new ProductCategory();
            $subcateg_PVC->setName("PVC");
            
            $product = new Product();
            $product->setDescription("220x200");
            $product->setName("Baie vitrée PVC");
            $product->setPrice(590.99);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);
            $manager->persist($product);

            $product = new Product();
            $product->setDescription("100x150");
            $product->setName("Baie vitrée PVC");
            $product->setPrice(380.59);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);
            $manager->persist($product);

            $product = new Product();
            $product->setDescription("300x200");
            $product->setName("Baie vitrée PVC");
            $product->setPrice(680.99);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);
            $manager->persist($product);

            $subcateg_PVC->setOffersProduct(true);
            $categ_baieVitree->addCategory($subcateg_PVC);
            $manager->persist($subcateg_PVC);
            /******************/


            /****** PVC-Alu *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("PVC-Alu");
            
            $product = new Product();
            $product->setDescription("110x150");
            $product->setName("Baie vitrée PVC-Alu");
            $product->setPrice(690.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x150");
            $product->setName("Baie vitrée PVC-Alu");
            $product->setPrice(470.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x250");
            $product->setName("Baie vitrée PVC-Alu");
            $product->setPrice(580.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_baieVitree->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            /******************/
            
            /****** Bois *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("Bois");
            
            $product = new Product();
            $product->setDescription("300x100");
            $product->setName("Baie vitrée Bois");
            $product->setPrice(850.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x100");
            $product->setName("Baie vitrée Bois");
            $product->setPrice(720.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("400x250");
            $product->setName("Baie vitrée Bois");
            $product->setPrice(1220.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_baieVitree->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            /******************/
            
            /****** Bois-Alu *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("Bois-Alu");
            
            $product = new Product();
            $product->setDescription("90x100");
            $product->setName("Baie vitrée Bois-Alu");
            $product->setPrice(220.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x100");
            $product->setName("Baie vitrée Bois-Alu");
            $product->setPrice(390.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("300x300");
            $product->setName("Baie vitrée Bois-Alu");
            $product->setPrice(450.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_baieVitree->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            $manager->persist($categ_baieVitree);
            /******************/
        }
        /********************************************************/

        
        /**************** PORTE ENTREE ****************/
        if (true) {
            /****** PVC *****/
            $subcateg_PVC = new ProductCategory();
            $subcateg_PVC->setName("PVC");
            
            $product = new Product();
            $product->setDescription("220x200");
            $product->setName("Porte fenêtre PVC");
            $product->setPrice(590.99);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);
            $manager->persist($product);

            $product = new Product();
            $product->setDescription("100x150");
            $product->setName("Porte fenêtre PVC");
            $product->setPrice(380.59);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);
            $manager->persist($product);

            $product = new Product();
            $product->setDescription("300x200");
            $product->setName("Porte fenêtre PVC");
            $product->setPrice(680.99);
            $product->setFkidProdcat($subcateg_PVC);
            $product->setPictures([$img]);
            $manager->persist($product);

            $subcateg_PVC->setOffersProduct(true);
            $categ_porteEntree->addCategory($subcateg_PVC);
            $manager->persist($subcateg_PVC);
            /******************/


            /****** PVC-Alu *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("PVC-Alu");
            
            $product = new Product();
            $product->setDescription("110x150");
            $product->setName("Porte d'entrée PVC-Alu");
            $product->setPrice(690.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x150");
            $product->setName("Porte d'entrée PVC-Alu");
            $product->setPrice(470.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x250");
            $product->setName("Porte d'entrée PVC-Alu");
            $product->setPrice(580.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_porteEntree->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            /******************/
            
            /****** Bois *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("Bois");
            
            $product = new Product();
            $product->setDescription("300x100");
            $product->setName("Porte d'entrée Bois");
            $product->setPrice(850.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x100");
            $product->setName("Porte d'entrée Bois");
            $product->setPrice(720.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("400x250");
            $product->setName("Porte d'entrée Bois");
            $product->setPrice(1220.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_porteEntree->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            /******************/
            
            /****** Bois-Alu *****/
            $productSubCateg = new ProductCategory();
            $productSubCateg->setName("Bois-Alu");
            
            $product = new Product();
            $product->setDescription("90x100");
            $product->setName("Porte d'entrée Bois-Alu");
            $product->setPrice(220.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("250x100");
            $product->setName("Porte d'entrée Bois-Alu");
            $product->setPrice(390.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);
            
            $product = new Product();
            $product->setDescription("300x300");
            $product->setName("Porte d'entrée Bois-Alu");
            $product->setPrice(450.99);
            $product->setFkidProdcat($productSubCateg);
            $product->setPictures([$img]);
            $manager->persist($product);

            $productSubCateg->setOffersProduct(true);
            $categ_porteEntree->addCategory($productSubCateg);
            $manager->persist($productSubCateg);
            $manager->persist($categ_porteEntree);
            /******************/
        }
        /********************************************************/
        
        $manager->flush();
    }
}
