<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"product_categ:read"}},
 *     collectionOperations={
 *        "get",
 *        "post"
 *     },
 *     itemOperations={
 *        "get",
 *        "put",
 *        "delete",
 *        "patch"
 *     }
 * ),
 *
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "name": "partial", "categories": "end"})
 * @ApiFilter(BooleanFilter::class, properties={"offersProduct"})
 * @ApiFilter(PropertyFilter::class)
 * @ORM\Entity(repositoryClass=ProductCategoryRepository::class)
 */
class ProductCategory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"product:read","product_categ:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Type("string")
     * @Groups({"product:read","product_categ:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\Type("boolean")
     * @Groups({"product:read","product_categ:read"})
     */
    private $offersProduct;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="fkIdProdCat", orphanRemoval=true)
     * @Groups({"product_categ:read"})
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity=ProductCategory::class)
     * @ORM\JoinTable(name="LinkCategory",
     *      joinColumns={@ORM\JoinColumn(name="parent", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="child", referencedColumnName="id")}
     *      )
     * @Groups({"product:read","product_categ:read"})
     */
    private $categories;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOffersProduct(): ?bool
    {
        return $this->offersProduct;
    }

    public function setOffersProduct(bool $offersProduct): self
    {
        $this->offersProduct = $offersProduct;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setFkidProdcat($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getFkidProdcat() === $this) {
                $product->setFkidProdcat(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(self $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(self $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }
}
